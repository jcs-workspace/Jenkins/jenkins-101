@echo off
:: ========================================================================
:: $File: build.bat $
:: $Date: 2023-10-21 23:55:12 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2023 by Shen, Jen-Chieh $
:: ========================================================================

cd ..

docker build -t jenkins-101:0.0.1 .

docker network create jenkins
