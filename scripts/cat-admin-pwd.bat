@echo off
:: ========================================================================
:: $File: cat-admin-pwd.bat $
:: $Date: 2023-10-22 00:04:17 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2023 by Shen, Jen-Chieh $
:: ========================================================================

cd ..

docker exec jenkins-blueocean cat /var/jenkins_home/secrets/initialAdminPassword
